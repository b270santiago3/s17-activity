



	


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	function printUserLogin(){
		let fullName = prompt("What is your name?");
		let howOld = prompt("How old are you? ");
		let whereLive = prompt("Where do you live? ");

		console.log("Hello, " + fullName);
		console.log("You are " + howOld + " years old.")
		console.log("You live in " + whereLive);
	}
	printUserLogin();	


	function myFavBand(){
		let oneBand = "1. The Beatles";

		function nestedBand(){
			let nestedTwoBand = "2. Metallica";
			let nestedThreeBand = "3. The Eagles";
			let nestedFourBand = "4. L'arc~en~Ciel";
			let nestedFiveBand = "5. Eraserheads";
			console.log(oneBand);
			console.log(nestedTwoBand);
			console.log(nestedThreeBand);
			console.log(nestedFourBand);
			console.log(nestedFiveBand);
		}
		nestedBand();
	}

	myFavBand();

	function favMovies(){
		let firstMovie = "1. The Godfather";
		let secondMovie = "2. The Godfather, Part II";
		let thirdMovie = "3. Shawshank Redemption";
		let fourthMovie = "4.To Kill A Mockingbird";
		let fifthMovie = "5. Psycho";
		console.log(firstMovie);
		console.log("Rotten Tomatoes Rating: 97%");
		console.log(secondMovie);
		console.log("Rotten Tomatoes Rating: 96%");
		console.log(thirdMovie);
		console.log("Rotten Tomatoes Rating: 91%");
		console.log(fourthMovie);
		console.log("Rotten Tomatoes Rating: 93%")
		console.log(fifthMovie);
		console.log("Rotten Tomatoes Rating: 96%")

	}
	favMovies();

printUsers();
	function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

};

printUsers();